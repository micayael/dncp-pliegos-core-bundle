<?php

namespace Dncp\Bundle\PliegosCore\Service;

use App\Entity\Pliego;
use Dncp\Bundle\PliegosCore\Exception\RedirectException;
use Symfony\Component\Translation\TranslatorInterface;

class PliegoValidator
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function validarClausula($clausula, $datos)
    {
        $clausulaRequerida = false === $clausula['opcional'] && false === $clausula['no_aplica'];
        $clausulaSoloLectura = !$clausula['editable'];
        $clausulaExcluida = $datos['excluida'] ?? false;
        $clausulaNoAplica = $datos['no_aplica'] ?? false;
        $clausulaValor = $datos['valor'] ?? null;

        // Si la clausula es obligaria y no tiene valor o,
        // si es opcional y no se encuentra marcada como excluída ni no_aplica y tampoco tiene valor
        if (
            $clausulaRequerida && null === $clausulaValor ||
            false === $clausulaRequerida && false === $clausulaExcluida && false === $clausulaNoAplica && null === $clausulaValor
        ) {
            return 'clausula.incompleta';
        }

        if (
            ($clausulaSoloLectura && $clausulaValor === $clausula['cuerpo']) ||
            (false === $clausulaRequerida && ($clausulaExcluida || $clausulaNoAplica)) ||
            (false === in_array($clausula['tipo_componente'], ['richtext']) && null !== $clausulaValor)
        ) {
            return 'clausula.definida';
        }

        return 'clausula.require_revision';
    }

    public function getClausulaValor($clausula, $datos)
    {
        if (isset($datos['no_aplica']) && true === $datos['no_aplica']) {
            return $this->translator->trans('no_aplica');
        }

        if (isset($datos['excluida']) && true === $datos['excluida']) {
            return $this->translator->trans('excluida');
        }

        if (!isset($datos['valor']) || '' === $datos['valor']) {
            return '';
        }

        switch ($clausula['tipo_componente']) {
            case 'text':
                $ret = $datos['valor'];
                break;

            case 'richtext':
                $ret = $datos['valor'];
                break;

            case 'integer':
                $ret = number_format($datos['valor'], 0, ',', '.');
                break;

            case 'percent':
                $ret = number_format($datos['valor'], 2, ',', '.').' %';
                break;

            case 'numeric':
                $ret = number_format($datos['valor'], 2, ',', '.');
                break;

            case 'boolean':
                $ret = $this->translator->trans($this->getBooleanTranslatorKey($datos['valor']));
                break;

            case 'choice':
                if($clausula['multiple'])
                {
                    $ret = implode(', ', $datos['valor']);
                    break;
                }else{
                    $ret = $datos['valor'];
                    break;
                }
            case 'currency':

                $monedas = PliegoFormManager::MONEDAS;

                $moneda = array_search($datos['valor'], $monedas);

                $ret = $moneda;
                break;

            case 'date':
                $ret = date('d/m/Y', strtotime($datos['valor']));
                break;

            case 'time':
                $ret = date('H:i', strtotime($datos['valor']));
                break;

            case 'email':
                $ret = $datos['valor'];
                break;

            case 'url':
                $ret = sprintf('<a href="%s" target="_blank">%s</a>', $datos['valor'], $datos['valor']);
                break;

            default:
                throw new \Exception('No se ha definido el tipo de componente de formulario: '.$clausula['tipo_componente']);
        }

        return $ret;
    }

    public function validarPliego(Pliego $pliego, array $plantillaActual, ?Pliego $pliegoAnterior): array
    {
        $convocatoriaSlug = $pliego->getConvocatoriaSlug();

        if (!$pliego->isBorrador()) {
            throw new RedirectException(
                'msg.pliego.no_puede_ser_confirmado',
                'pliego.resumen',
                ['convocatoriaSlug' => $convocatoriaSlug]
            );
        }

        $this->validateCambioPlantilla($pliego, $plantillaActual, $pliegoAnterior);

        $plantillaEstadistica = [
            'falta_definicion' => 0,
            'requiren_revision' => 0,
            'definidas' => 0,
            'no_aplican' => 0,
            'excluidas' => 0,
            'modificadas' => 0,
        ];

        $estadistica['totales'] = $plantillaEstadistica;

        $plantilla = $pliego->getPlantilla();
        $datosActuales = $pliego->getDatos();

        $datosAnteriores = [];

        // En caso de ser la versión 1 siempre irá permitirá ver las estadísticas
        $pliegoModificado = true;

        // Si ya existen versiones anteriores busca el pliego anterior para comparar contra los hashes de las cláusulas
        // si fueron modificadas
        if ($pliegoAnterior) {
            $datosAnteriores = $pliegoAnterior->getDatos();

            // Si existen versiones anteriores se validará que este intento de confirmación contenga modificaciones
            $pliegoModificado = false;
        }

        // Recorre la plantilla y por cada sección calcula las estadísticas
        foreach ($plantilla as $seccionId => $seccion) {
            $estadistica[$seccion['nombre']] = $plantillaEstadistica;

            foreach ($seccion['clausulas'] as $clausula) {
                $clausulaId = $clausula['identificador'];

                $clausulaActualDatos = $datosActuales[$clausulaId];

                // Solo en caso de existir pliegos anteriores calcula la estadística de cláusulas modificadas
                if ($pliegoAnterior) {
                    $clausulaAnteriorDatos = $datosAnteriores[$clausulaId];

                    if ($clausulaActualDatos['hash'] !== $clausulaAnteriorDatos['hash']) {
                        ++$estadistica['totales']['modificadas'];
                        ++$estadistica[$seccion['nombre']]['modificadas'];

                        $pliegoModificado = true;
                    }
                }

                if (isset($clausulaActualDatos['no_aplica']) && $clausulaActualDatos['no_aplica']) {
                    ++$estadistica['totales']['no_aplican'];
                    ++$estadistica[$seccion['nombre']]['no_aplican'];
                }

                if (isset($clausulaActualDatos['excluida']) && $clausulaActualDatos['excluida']) {
                    ++$estadistica['totales']['excluidas'];
                    ++$estadistica[$seccion['nombre']]['excluidas'];
                }

                $validacion = $this->validarClausula($clausula, $clausulaActualDatos);

                if ('clausula.finalizada' === $validacion) {
                    ++$estadistica['totales']['definidas'];
                    ++$estadistica[$seccion['nombre']]['definidas'];
                } elseif ('clausula.incompleta' === $validacion) {
                    ++$estadistica['totales']['falta_definicion'];
                    ++$estadistica[$seccion['nombre']]['falta_definicion'];
                } else {
                    ++$estadistica['totales']['requiren_revision'];
                    ++$estadistica[$seccion['nombre']]['requiren_revision'];
                }
            }
        }

        if (!$pliegoModificado) {
            throw new RedirectException(
                'msg.pliego.sin_modificaciones',
                'pliego.resumen',
                ['convocatoriaSlug' => $convocatoriaSlug]
            );
        }

        return $estadistica;
    }

    public function validarNuevaVersionClausula(array $clausulasActuales, array $clausulasNuevas, string $clausulaId)
    {
        $clausulaActual = $clausulasActuales[$clausulaId] ?? null;
        $clausulaNueva = $clausulasNuevas[$clausulaId] ?? null;

        if($clausulaNueva){

            if(!$clausulaActual){
                return 'plantilla.clausula.nueva';
            }

            if($clausulaActual['hash'] != $clausulaNueva['hash']){
                return 'plantilla.clausula.modificada';
            }

            if($clausulaActual['hash'] == $clausulaNueva['hash']){
                return 'plantilla.clausula.igual';
            }

        }elseif(!$clausulaNueva and $clausulaActual){

            return 'plantilla.clausula.eliminada';

        }
    }

    private function validateCambioPlantilla(Pliego $pliego, array $plantillaActual, ?Pliego $pliegoAnterior): void
    {
        $existeNuevaPlantilla = false;

        if($pliego->getPlantillaVersion() !== $plantillaActual['version']){

            throw new RedirectException(
                null,
                'pliego.actualizar_plantilla',
                [
                    'convocatoriaSlug' => $pliego->getConvocatoriaSlug(),
                    'version' => $pliego->getVersion(),
                ]
            );

        }
    }

    private function getBooleanTranslatorKey($value)
    {
        if (true === $value) {
            return 'boolean.true';
        } elseif (false === $value) {
            return 'boolean.false';
        } else {
            return 'boolean.undefined';
        }
    }
}
