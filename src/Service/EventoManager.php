<?php

namespace Dncp\Bundle\PliegosCore\Service;

use App\Dao\Sicp\ConvocatoriaDao;
use App\Entity\HistorialEvento;
use App\Entity\Pliego;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EventoManager
{
    private $em;

    private $convocatoriaDao;

    public function __construct(EntityManagerInterface $em, ConvocatoriaDao $convocatoriaDao)
    {
        $this->em = $em;
        $this->convocatoriaDao = $convocatoriaDao;
    }

    /**
     * Agrega un evento al historial de eventos. En caso de ser un evento de modificación evalúa si efectivamente hubo
     * alguna modificación al pliego.
     *
     * @param Pliego $pliego
     * @param string $tipoEvento Cualquier valor de HistorialEvento::EVENTO_*
     * @param bool   $flush      Indica si debe o no realizar un flush al entity manager
     *
     * @return bool
     */
    public function addEvento(Pliego $pliego, string $tipoEvento, $flush = false): bool
    {
        // Se agregan eventos de modificación solo si existen modificaciones reales en los datos
        if (HistorialEvento::EVENTO_UPDATE === $tipoEvento) {
            $uow = $this->em->getUnitOfWork();
            $uow->computeChangeSets();

            if (false === $uow->isEntityScheduled($pliego)) {
                return false;
            }
        }

        $evento = new HistorialEvento();

        $evento->setPliego($pliego);
        $evento->setEvento($tipoEvento);

        $this->em->persist($evento);

        if ($flush) {
            $this->em->flush();
        }

        return true;
    }

    public function getPliegoFromEvento(HistorialEvento $evento)
    {
        $pliego = $evento->getPliego();

        // Se obtiene la convocatoria relacionada al pliego
        $convocatoria = $this->convocatoriaDao->getConvocatoria($pliego->getConvocatoriaSlug());

        if (!$convocatoria) {
            throw new NotFoundHttpException();
        }

        $pliego->setConvocatoria($convocatoria);

        return $pliego;
    }
}
