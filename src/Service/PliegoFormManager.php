<?php

namespace Dncp\Bundle\PliegosCore\Service;

use App\Entity\Pliego;
use App\Form\PliegoType;
use App\Form\ClausulaType;
use Dncp\Bundle\PliegosCore\Exception\PliegoInputNotDefinedException;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Time;
use Symfony\Component\Validator\Constraints\Url;

class PliegoFormManager
{
    const MONEDAS = [
        'Guaraníes' => 'PYG',
        'Dólares' => 'USD',
    ];

    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * Crea un formulario PliegoType agregando cada clausula como un ClausulaType.
     *
     * @param array $pliego
     * @param array $clausulas
     *
     * @return \Symfony\Component\Form\FormInterface
     *
     * @throws PliegoInputNotDefinedException
     */
    public function createPliegoForm(Pliego $pliego, array $clausulas)
    {
        $defaultData = [];

        $form = $this->formFactory->create(PliegoType::class, $defaultData);

        $pliegoDatos = $pliego->getDatos();

        foreach ($clausulas as $clausula) {
            $clausulaDatos = $pliegoDatos[$clausula['identificador']] ?? [];

            $this->addClausulaType($form, $clausula, $clausulaDatos);
        }

        return $form;
    }

    /**
     * Agrega cada cláusula al formulario PliegoType como una ClausulaType que puede contener, además del input de dato,
     * los inputs para excluir o no aplicar la cláusula.
     *
     * @param Form  $pliegoForm
     * @param array $clausula
     */
    private function addClausulaType(Form $pliegoForm, array $clausula, array $clausulaDatos)
    {
        $clausulaId = $clausula['identificador'];

        $pliegoForm->add(
            $clausulaId,
            ClausulaType::class,
            [
                'label' => $clausula['titulo'],
            ]
        );

        $clausulaForm = $pliegoForm->get($clausulaId);

        // Agrega un campo para excluir esta cláusula si la plantilla lo permite
        if ($clausula['opcional']) {
            $clausulaForm->add(
                'excluir',
                CheckboxType::class,
                [
                    'label' => 'clausula.excluir',
                    'required' => false,
                    'attr' => ['class' => 'clausula-excluir'],
                    'data' => $clausulaDatos['excluida'] ?? false,
                    'label_attr' => ['class' => 'checkbox-custom'],
                ]
            );
        }

        // Agrega un campo para marcar la cláusula como NO APLICA si la plantilla lo permite
        if ($clausula['no_aplica']) {
            $clausulaForm->add(
                'no_aplica',
                CheckboxType::class,
                [
                    'label' => 'clausula.no_aplica',
                    'required' => false,
                    'attr' => ['class' => 'clausula-no-aplica'],
                    'data' => $clausulaDatos['no_aplica'] ?? false,
                    'label_attr' => ['class' => 'checkbox-custom'],
                ]
            );
        }

        $options = [
            'required' => false,
            'label' => false,
        ];

        $inputType = null;

        switch ($clausula['tipo_componente']) {
            case 'text':
                $inputType = TextType::class;

                // Carga el input con el texto almacenado, de lo contrario lo carga con el texto definido en la plantilla
                $options['data'] = $clausulaDatos['valor'] ?? null;

                break;

            case 'richtext':
                $inputType = CKEditorType::class;

                // Carga el input con el texto almacenado, de lo contrario lo carga con el texto definido en la plantilla
                $options['data'] = $clausulaDatos['valor'] ?? $clausula['cuerpo'];

                if ($clausula['editable']) {
                    $options['attr'] = [
                        'class' => 'richtext',
                    ];
                }

                break;

            case 'integer':
                $inputType = IntegerType::class;

                // Carga el input con el texto almacenado, de lo contrario lo carga con el texto definido en la plantilla
                $options['data'] = $clausulaDatos['valor'] ?? null;

                // Agrega la validación de número mínimo si la plantilla lo define
                if ($value = $clausula['minimo']) {
                    $options['constraints'][] = new GreaterThanOrEqual($value);
                }

                // Agrega la validación de número máximo si la plantilla lo define
                if ($value = $clausula['maximo']) {
                    $options['constraints'][] = new LessThanOrEqual($value);
                }

                break;

            case 'percent':
                $inputType = NumberType::class;

                // Carga el input con el texto almacenado, de lo contrario lo carga con el texto definido en la plantilla
                $options['data'] = $clausulaDatos['valor'] ?? null;

                // Agrega la validación de número mínimo si la plantilla lo define
                if ($value = $clausula['minimo']) {
                    $options['constraints'][] = new GreaterThanOrEqual($value);
                }

                // Agrega la validación de número máximo si la plantilla lo define
                if ($value = $clausula['maximo']) {
                    $options['constraints'][] = new LessThanOrEqual($value);
                }

                break;
            case 'numeric':
                $inputType = NumberType::class;

                // Carga el input con el texto almacenado, de lo contrario lo carga con el texto definido en la plantilla
                $options['data'] = $clausulaDatos['valor'] ?? null;

                // Agrega la validación de número mínimo si la plantilla lo define
                if ($value = $clausula['minimo']) {
                    $options['constraints'][] = new GreaterThanOrEqual($value);
                }

                // Agrega la validación de número máximo si la plantilla lo define
                if ($value = $clausula['maximo']) {
                    $options['constraints'][] = new LessThanOrEqual($value);
                }

                break;

            case 'boolean':
                $inputType = ChoiceType::class;

                // Si existe valor almacenado lo carga. Se permiten 3 valores: true, false y null
                if (isset($clausulaDatos['valor']) && true === $clausulaDatos['valor']) {
                    $options['data'] = true;
                } elseif (isset($clausulaDatos['valor']) && false === $clausulaDatos['valor']) {
                    $options['data'] = false;
                } else {
                    $options['data'] = null;
                }

                // Agrega los 3 valores posibles. Se debe prestar atención para no confundir null con false
                $options['choices'] = [
                    'Si' => true,
                    'No' => false,
                ];

                $options['placeholder'] = 'Seleccione una opción';
//                $options['attr'] = ['class' => 'custom-select'];
                $options['choice_translation_domain'] = false;

                break;

            case 'choice':
                $inputType = ChoiceType::class;

                $options['data'] = $clausulaDatos['valor'] ?? null;

                if ($clausula['opciones']) {
                    $lineBreak = "\n";

                    if (false !== strpos($clausula['opciones'], "\r\n")) {
                        $lineBreak = "\r\n";
                    }

                    $opciones = explode($lineBreak, $clausula['opciones']);

                    $options['choices'] = array_merge(array_combine($opciones, $opciones));
                }

                if ($clausula['multiple']) {
                    $options['multiple'] = true;
                }

                $options['placeholder'] = 'Seleccione una opción';
//                $options['attr'] = ['class' => 'custom-select'];
                $options['choice_translation_domain'] = false;

                break;

            case 'currency':
                $inputType = ChoiceType::class;

                $options['data'] = $clausulaDatos['valor'] ?? null;

                // Agrega los 3 valores posibles. Se debe prestar atención para no confundir null con false
                $options['choices'] = self::MONEDAS;

                if ($clausula['multiple']) {
                    $options['multiple'] = true;
                }

                $options['placeholder'] = 'Seleccione una opción';
//                $options['attr'] = ['class' => 'custom-select'];
                $options['choice_translation_domain'] = false;

                break;

            case 'date':
                $inputType = DateType::class;

                $options['data'] = $clausulaDatos['valor'] ?? null;

                $options['input'] = 'string';
                $options['widget'] = 'single_text';
                $options['format'] = 'dd/MM/yyyy';
                $options['attr'] = [
                    'placeholder' => 'dd/mm/aaaa',
                ];

                $options['attr'] = ['data-inputmask' => "'alias': 'datetime', 'inputformat': 'dd/mm/yyyy', 'clearIncomplete': true, 'placeholder': 'dd/mm/aaaa'"];

                $options['constraints'][] = new Date();

                break;
            case 'time':
                $inputType = TimeType::class;

                $options['data'] = $clausulaDatos['valor'] ?? null;

                $options['widget'] = 'single_text';
                $options['with_seconds'] = false;
                $options['input'] = 'string';

                $options['constraints'][] = new Time();

                break;
            case 'email':
                $inputType = EmailType::class;

                $options['data'] = $clausulaDatos['valor'] ?? null;

                $options['constraints'][] = new Email();

                break;
            case 'url':
                $inputType = UrlType::class;

                $options['data'] = $clausulaDatos['valor'] ?? null;

                $options['constraints'][] = new Url();

                break;
        }

        if (!$inputType) {
            throw new PliegoInputNotDefinedException('No se ha definido el tipo de componente de formulario: '.$clausula['tipo_componente']);
        }

        $clausulaForm->add('valor', $inputType, $options);
    }

    /**
     * Procesa los datos obtenidos en el formulario y devuelve los datos de las cláusulas como un array.
     *
     * @param Form  $form
     * @param array $clausulas
     */
    public function procesarClausulas(FormInterface $form, array $clausulas)
    {
        $data = $form->getData();

        /**
         * Será el array que contendrá las cláusulas a guardar.
         *
         * @var array $clausulasFinales
         */
        $clausulasFinales = [];

        foreach ($clausulas as $clausula) {
            $clausulaId = $clausula['identificador'];

            // En caso de que entre los datos no exista este valor se inserta el valor original definido en la plantilla
            if (!isset($data[$clausulaId])) {
                $clausulaValor = $clausula['cuerpo'];

                $clausulasFinales[$clausulaId]['valor'] = $clausulaValor;
                $clausulasFinales[$clausulaId]['hash'] = $this->createClausulaHash($clausulaId, (string) $clausulaValor);

                continue;
            }

            // en caso de la que cláusula sea opcional y se haya exluído la elimina
            if ($clausula['opcional'] && $data[$clausulaId]['excluir']) {
                $clausulasFinales[$clausulaId]['excluida'] = true;
                $clausulasFinales[$clausulaId]['hash'] = $this->createClausulaHash($clausulaId, 'excluida');

                continue;
            }

            // En caso de que la cláusula se haya marcado como no aplica
            if ($clausula['no_aplica'] && $data[$clausulaId]['no_aplica']) {
                $clausulasFinales[$clausulaId]['no_aplica'] = true;
                $clausulasFinales[$clausulaId]['hash'] = $this->createClausulaHash($clausulaId, 'no_aplica');

                continue;
            }

            // En caso de que la cláusula sea de solo lectura mantiene su valor original definido en la plantilla
            if (!$clausula['editable']) {
                $clausulaValor = $clausula['cuerpo'];

                $clausulasFinales[$clausulaId]['valor'] = $clausulaValor;
                $clausulasFinales[$clausulaId]['hash'] = $this->createClausulaHash($clausulaId, (string) $clausulaValor);

                continue;
            }

            // En caso de que la cláusula sea editable se agrega el valor definido por el usuario
            $clausulaValor = $data[$clausulaId]['valor'];

            if (is_string($clausulaValor)) {
                if (empty($clausulaValor)) {
                    $clausulaValor = null;
                } else {
                    $invalidChars = ['“', '”', '–', '…', 'ĺ'];
                    $clausulaValor = str_replace($invalidChars, '', $clausulaValor);
                }
            }

            $clausulasFinales[$clausulaId]['valor'] = $clausulaValor;

            if (is_array($clausulaValor)) {
                $valor = json_encode($clausulaValor);
            } else {
                $valor = (string) $clausulaValor;
            }

            $clausulasFinales[$clausulaId]['hash'] = $this->createClausulaHash($clausulaId, $valor);
        }

        return $clausulasFinales;
    }

    /**
     * Crea un hash para una clásula a partir de sus datos actuales. Solo se toman en cuenta los datos en sí
     * de la cláusula para asegurar que si no hubo modificación permanezca el mismo hash.
     *
     * @param string $clausulaIdentificador
     * @param string $clausulaValor
     *
     * @return string
     */
    private function createClausulaHash(string $clausulaIdentificador, string $clausulaValor): string
    {
        return sha1($clausulaIdentificador.'|'.$clausulaValor);
    }
}
