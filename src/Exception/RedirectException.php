<?php

namespace Dncp\Bundle\PliegosCore\Exception;

class RedirectException extends SicpException
{
    private $route;

    private $routeParams;

    private $messageType;

    /**
     * RedirectException constructor.
     *
     * @param string $message
     * @param string $route
     * @param array  $routeParams
     */
    public function __construct(?string $message, string $route, array $routeParams = [], string $messageType = 'warning')
    {
        parent::__construct($message, 0, null);

        $this->route = $route;
        $this->routeParams = $routeParams;
        $this->messageType = $messageType;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * @return string
     */
    public function getMessageType(): string
    {
        return $this->messageType;
    }
}
