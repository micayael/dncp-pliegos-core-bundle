<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

interface WebInterface extends SicpInterface
{
    public function getTemplate(): string;
}
