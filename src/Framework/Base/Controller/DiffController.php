<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use App\Dao\Sicp\ConvocatoriaDao;
use App\Entity\Pliego;
use App\Form\CompararVersionesType;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

abstract class DiffController extends BaseController implements WebInterface, PageInterface
{
    abstract protected function getVersiones($convocatoriaSlug);

    abstract protected function getPliego1(array $data): Pliego;

    abstract protected function getPliego2(array $data): Pliego;

    abstract protected function getObservaciones(array $requestParams): array;

    public function __invoke(Request $request, Breadcrumbs $breadcrumbs, ConvocatoriaDao $convocatoriaDao)
    {
        $requestParams = [
            'convocatoriaSlug' => $request->get('convocatoriaSlug'),
            'version1' => $request->get('version1'),
            'version2' => $request->get('version2'),
        ];

        $form = $this->getVersionesForm($requestParams);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute($request->get('_route'), [
                'convocatoriaSlug' => $requestParams['convocatoriaSlug'],
                'version1' => $data['version1'],
                'version2' => $data['version2'],
            ]);
        }

        $newPliego = $this->getPliego1($requestParams);

        $this->throw404IfNotFound($newPliego);

        $oldPliego = $this->getPliego2($requestParams);

        $this->throw404IfNotFound($oldPliego);

        // Se obtiene la convocatoria relacionada al pliego
        $convocatoria = $convocatoriaDao->getConvocatoria($requestParams['convocatoriaSlug']);

        $this->throw404IfNotFound($convocatoria);

        $newPliego->setConvocatoria($convocatoria);
        $oldPliego->setConvocatoria($convocatoria);

        // Se obtiene la sección actual desde la URL, default: la primera sección
        $seccionId = $request->get('seccion', key($newPliego->getPlantilla()));

        $seccionActual = $newPliego->getPlantilla()[$seccionId];
        $seccionActual2 = $oldPliego->getPlantilla()[$seccionId];

        $clausulasIndice = [];

        foreach ($seccionActual2['clausulas'] as $clausula) {

            $clausulasIndice[$clausula['identificador']] = $clausula;

        }

        foreach ($seccionActual['clausulas'] as $clausula) {

            $clausulasIndice[$clausula['identificador']] = $clausula;

        }

        $observaciones = $this->getObservaciones($requestParams);

        $data = array_merge($requestParams, [
            'convocatoria' => $convocatoria,
            'newPliego' => $newPliego,
            'oldPliego' => $oldPliego,
            'clausulasIndice' => $clausulasIndice,
            'seccionActual' => $seccionActual,
            'observaciones' => $observaciones,
            'form' => $form->createView(),
        ]);

        $this->getBreadcrumbs($request, $breadcrumbs, $data);

        return $this->render($this->getTemplate(), $data);
    }

    protected function getBreadcrumbs(Request $request, Breadcrumbs $breadcrumbs, array $data = []): void
    {
        $breadcrumbs->addItem('breadcrumb.mis_pliegos', $this->get('router')->generate('pliego.index'));

        $breadcrumbs->addRouteItem('breadcrumb.resumen', 'pliego.resumen', [
            'convocatoriaSlug' => $data['convocatoria']['slug'],
        ], null, [
            '%convocatoria_nombre%' => $data['convocatoria']['nombre'],
        ]);

        $this->setBreadcrumbs($request, $breadcrumbs, $data);

        $breadcrumbs->addItem('breadcrumb.comparador');
    }

    protected function getVersionesForm(array $data)
    {
        $versiones = $this->getVersiones($data['convocatoriaSlug']);

        $form = $this->createForm(CompararVersionesType::class, [
            'version1' => $data['version1'],
            'version2' => $data['version2'],
        ], ['versiones' => $versiones]);

        return $form;
    }

    public function getTemplate(): string
    {
        return 'diff/diff.html.twig';
    }

    public function getContextData(Request $request)
    {
    }

    public function getContextName(): ?string
    {
        return null;
    }
}
