<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class ActionController extends BaseController
{
    abstract protected function do(Request $request): array;

    abstract protected function getAfterRedirection(Request $request, array $data = []): RedirectResponse;

    public function __invoke(Request $request)
    {
        $data = $this->do($request);

        if (empty($data)) {
            $data = [];
        }

        if ($data instanceof RedirectResponse) {
            return $data;
        }

        return $this->getAfterRedirection($request, $data);
    }
}
