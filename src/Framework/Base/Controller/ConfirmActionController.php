<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

abstract class ConfirmActionController extends BaseController implements WebInterface, PageInterface
{
    const FORM_METHOD_POST = 'POST';
    const FORM_METHOD_PUT = 'PUT';
    const FORM_METHOD_DELETE = 'DELETE';

    abstract protected function getFormMethod(): string;

    /**
     * @param Request       $request
     * @param FormInterface $form
     * @param $contextData
     *
     * @return array
     *
     * @throws \Dncp\Bundle\PliegosCore\Exception\RedirectException
     */
    abstract protected function do(Request $request, FormInterface $form, $contextData, array $extraData): array;

    abstract protected function getPostRedirection(Request $request, array $data): RedirectResponse;

    abstract protected function getSubmitActions(FormBuilderInterface $formBuilder): FormBuilderInterface;

    public function __invoke(Request $request, Breadcrumbs $breadcrumbs)
    {
        $contextData = $this->getContextData($request);

        if ($contextData instanceof RedirectResponse) {
            return $contextData;
        }

        $this->throw404IfNotFound($contextData);

        $extraData = $this->getExtraData($request, $contextData);

        $formBuilder = $this->getFormBuilder($request);

        $form = $this->getActionForm($request, $formBuilder);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $postActionData = $this->do($request, $form, $contextData, $extraData);

            $data = array_merge([
                $this->getContextName() => $contextData,
            ], $extraData, $postActionData);

            return $this->getPostRedirection($request, $data);
        }

        $data = array_merge([
            $this->getContextName() => $contextData,
            'form' => $form->createView(),
        ], $extraData);

        $this->setBreadcrumbs($request, $breadcrumbs, $data);

        return $this->render($this->getTemplate(), $data);
    }

    protected function getFormBuilder(Request $request): FormBuilderInterface
    {
        $formMethod = $this->getFormMethod();

        if (!in_array($formMethod, [self::FORM_METHOD_POST, self::FORM_METHOD_PUT, self::FORM_METHOD_DELETE])) {
            throw new \Exception(
                'El formulario solo puede ser enviado por los siguientes métodos self::FORM_METHOD_POST | self::FORM_METHOD_PUT | self::FORM_METHOD_DELETE'
            );
        }

        return $this->createFormBuilder()
            ->setMethod($this->getFormMethod())
            ;
    }

    protected function getActionForm(Request $request, FormBuilderInterface $formBuilder): FormInterface
    {
        return $this->getSubmitActions($formBuilder)->getForm();
    }

    protected function getExtraData(Request $request, $contextData): array
    {
        return [];
    }
}
