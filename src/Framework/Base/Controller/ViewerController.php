<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

abstract class ViewerController extends BaseController implements WebInterface, PageInterface
{
    public function __invoke(Request $request, Breadcrumbs $breadcrumbs)
    {
        $contextData = $this->getContextData($request);

        if ($contextData instanceof RedirectResponse) {
            return $contextData;
        }

        $this->throw404IfNotFound($contextData);

        $data = array_merge([
            $this->getContextName() => $contextData,
        ], $this->getExtraData($request, $contextData));

        $this->setBreadcrumbs($request, $breadcrumbs, $data);

        return $this->render($this->getTemplate(), $data);
    }

    protected function getExtraData(Request $request, $contextData): array
    {
        return [];
    }
}
