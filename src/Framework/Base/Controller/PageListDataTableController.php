<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Omines\DataTablesBundle\DataTable;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

abstract class PageListDataTableController extends ListController implements PageInterface
{
    use DataTablesTrait;

    abstract public function getDataTable(Request $request): DataTable;

    protected function getListData(Request $request, $contextData): array
    {
    }

    public function __invoke(Request $request, Breadcrumbs $breadcrumbs)
    {
        $contextData = $this->getContextData($request);

        if ($this->getContextName()) {
            $this->throw404IfNotFound($contextData);
        }

        $table = $this->getDataTable($request)
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        $data['datatable'] = $table;

        $data = [
            'listName' => $this->getListName(),
            'datatable' => $table,
        ];

        if ($this->getContextName()) {
            $data[$this->getContextName()] = $contextData;
        }

        $data = array_merge($data, $this->getExtraData($request));

        $this->setBreadcrumbs($request, $breadcrumbs, $data);

        return $this->render($this->getTemplate(), $data);
    }
}
