<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

interface PageInterface
{
    public function setBreadcrumbs(Request $request, Breadcrumbs $breadcrumbs, array $data = []): void;

    public function getContextData(Request $request);

    public function getContextName(): ?string;
}
