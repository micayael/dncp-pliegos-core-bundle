<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

abstract class PageListController extends ListController implements PageInterface
{
    public function __invoke(Request $request, Breadcrumbs $breadcrumbs)
    {
        $contextData = $this->getContextData($request);

        if ($contextData instanceof RedirectResponse) {
            return $contextData;
        }

        if ($this->getContextName()) {
            $this->throw404IfNotFound($contextData);
        }

        $listData = $this->getListData($request, $contextData);

        $data[$this->getListName()] = $listData;

        if ($this->getContextName()) {
            $data[$this->getContextName()] = $contextData;
        }

        $data = array_merge($data, $this->getExtraData($request));

        $this->setBreadcrumbs($request, $breadcrumbs, $data);

        return $this->render($this->getTemplate(), $data);
    }
}
