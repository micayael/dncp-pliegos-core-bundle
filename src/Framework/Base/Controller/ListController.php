<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Controller;

use Symfony\Component\HttpFoundation\Request;

abstract class ListController extends BaseController implements WebInterface
{
    abstract protected function getListName(): string;

    abstract protected function getListData(Request $request, $contextData): array;

    protected function getExtraData(Request $request): array
    {
        return [];
    }
}
