<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Test;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class BaseWebTestCase extends WebTestCase
{
    /**
     * Obtiene un parámetro del container.
     */
    protected function getParameter(string $parameter): string
    {
        return $this->getContainer()->getParameter($parameter);
    }

    protected function getEntityManager()
    {
        return $this->getContainer()
            ->get('doctrine.orm.default_entity_manager');
    }
}
