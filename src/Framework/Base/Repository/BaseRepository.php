<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Repository;

use Dncp\Bundle\PliegosCore\Traits\ControlUnidadContratacionUserTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class BaseRepository extends ServiceEntityRepository
{
    use ControlUnidadContratacionUserTrait;
}
