<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Dao;

use Dncp\Bundle\PliegosCore\Traits\ControlUnidadContratacionUserTrait;

abstract class BaseDbDao
{
    use ControlUnidadContratacionUserTrait;
}
