<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\Dao;

use Micayael\PHPExtras\Functions\StringFunctions;

abstract class BaseApiDao
{
    /**
     * Agrega los parámetros a una url. Los parámetros pueden ser.
     *
     * @param $url
     * @param array $pathParams  Parámetros
     * @param array $queryParams
     *
     * @return string
     */
    protected function addParamsToUrl($url, array $pathParams = [], array $queryParams = [])
    {
        return StringFunctions::formatUrl($url, $pathParams, $queryParams);
    }
}
