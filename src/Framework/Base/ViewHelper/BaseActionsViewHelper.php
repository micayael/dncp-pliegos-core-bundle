<?php

namespace Dncp\Bundle\PliegosCore\Framework\Base\ViewHelper;

use Nzo\UrlEncryptorBundle\UrlEncryptor\UrlEncryptor;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\TranslatorInterface;

abstract class BaseActionsViewHelper
{
    abstract protected function getActions($data, array $actions);

    protected $router;
    protected $translator;
    protected $authorizationChecker;
    protected $twig;
    protected $urlEncryptor;

    public function __construct(
        RouterInterface $router,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker,
        \Twig_Environment $twig,
        UrlEncryptor $urlEncryptor
    ) {
        $this->router = $router;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
        $this->twig = $twig;
        $this->urlEncryptor = $urlEncryptor;
    }

    /**
     * Crea el código HTML para el combo de acciones de una lista.
     *
     * @param mixed $data
     *
     * @return string
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function create($data, $inline = false)
    {
        $actions = $this->getActions($data, []);

        $template = 'bases/partial/view_helper.html.twig';

        if ($inline) {
            $template = 'bases/partial/view_helper_inline.html.twig';
        }

        return $this->twig->render($template, [
            'actions' => $actions,
        ]);
    }

    /**
     * Retorna la plantilla a utilizar.
     *
     * @return string
     */
    protected function getTemplate()
    {
        return 'bases/partial/view_helper.html.twig';
    }

    /**
     * Helper para crear un action aplicando la seguridad básica.
     *
     * @param $text
     * @param $role
     * @param $route
     * @param array $params
     * @param null  $type
     * @param bool  $divider
     *
     * @return array
     */
    protected function createAction($text, $role, $route, $params = [], $type = null, $divider = false)
    {
        $action = [
            'text' => $this->translator->trans($text),
            'url' => $this->router->generate($route, $params),
            'role' => $role,
        ];

        if ($type) {
            $action['type'] = $type;
        }

        if ($divider) {
            $action['divider'] = $divider;
        }

        return $action;
    }

    protected function arrayKeysToSnakeCase(array $array)
    {
        foreach ($array as $key => $value) {
            if (is_array($array[$key])) {
                $array[$key] = $this->arrayKeysToSnakeCase($array[$key]);
            }

            unset($array[$key]);
            $key = strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $key));
            $array[$key] = $value;
        }

        return $array;
    }
}
