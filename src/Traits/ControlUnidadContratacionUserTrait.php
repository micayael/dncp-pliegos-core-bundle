<?php

namespace Dncp\Bundle\PliegosCore\Traits;

use App\Security\UserMock;
use Dncp\CasAuthenticatorBundle\Security\User\CasUser;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait ControlUnidadContratacionUserTrait
{
    /**
     * @var CasUser $user
     */
    private $user;

    private $userMock;

    /**
     * @required
     */
    public function setUserMock(UserMock $userMock)
    {
        $this->userMock = $userMock;
    }

    /**
     * @required
     */
    public function setUser(TokenStorageInterface $tokenStorage)
    {
        if ($tokenStorage && $tokenStorage->getToken()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
    }

    /**
     * Obtiene el usuario en sesión y veririfica que sea un CasUser. En caso de no serlo significa que está en el
     * ambiente de testing por lo que utiliza un userMock que simula a la autenticación en CAS.
     *
     * @return UserMock|CasUser
     */
    protected function getUser()
    {
        if ($this->user instanceof CasUser) {
            return $this->user;
        }

        return $this->userMock;
    }
}
