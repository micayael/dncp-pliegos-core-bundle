<?php

namespace Dncp\Bundle\PliegosCore\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Blameable Trait, usable with PHP >= 5.4.
 *
 * @author David Buchmann <mail@davidbu.ch>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait BlameableEntity
{
    /**
     * @var string
     * @Gedmo\Blameable(on="create")
     * @ORM\Column(name="usuario_alta", nullable=true)
     */
    protected $usuarioAlta;

    /**
     * @var string
     * @Gedmo\Blameable(on="update")
     * @ORM\Column(name="usuario_modificacion", nullable=true)
     */
    protected $usuarioModificacion;

    /**
     * @return string
     */
    public function getUsuarioAlta()
    {
        return $this->usuarioAlta;
    }

    /**
     * @param string $usuarioAlta
     *
     * @return BlameableEntity
     */
    public function setUsuarioAlta(string $usuarioAlta): BlameableEntity
    {
        $this->usuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsuarioModificacion()
    {
        return $this->usuarioModificacion;
    }

    /**
     * @param string $usuarioModificacion
     *
     * @return BlameableEntity
     */
    public function setUsuarioModificacion(string $usuarioModificacion): BlameableEntity
    {
        $this->usuarioModificacion = $usuarioModificacion;

        return $this;
    }
}
