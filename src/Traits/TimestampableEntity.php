<?php

namespace Dncp\Bundle\PliegosCore\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Timestampable Trait, usable with PHP >= 5.4.
 *
 * @author Gediminas Morkevicius <gediminas.morkevicius@gmail.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait TimestampableEntity
{
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="fecha_modificacion", type="datetime")
     */
    protected $fechaModificacion;

    /**
     * @return \DateTime
     */
    public function getFechaAlta(): \DateTime
    {
        return $this->fechaAlta;
    }

    /**
     * @param \DateTime $fechaAlta
     *
     * @return TimestampableEntity
     */
    public function setFechaAlta(\DateTime $fechaAlta): TimestampableEntity
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFechaModificacion(): \DateTime
    {
        return $this->fechaModificacion;
    }

    /**
     * @param \DateTime $fechaModificacion
     *
     * @return TimestampableEntity
     */
    public function setFechaModificacion(\DateTime $fechaModificacion): TimestampableEntity
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }
}
